# Knockback Sticks

Small little experiment I made with command blocks in Minecraft 1.15.2

Just extract the `Knockback_Sticks.zip` file into your minecraft saves file to play the world.

This is a multiplayer world, so it would be best to extract it at the root of your server and rename the folder to `world` or whatever you chose your `level-name` to be in `server.properties`.

I recommend having your difficulty set to peaceful, because there isn't a command block to stop your hunger from dropping.